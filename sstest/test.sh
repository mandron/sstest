#!/bin/bash

test_status () {
status=$(echo $1 | grep HTTP/1.0 | awk {'print $2'})
if [ $status==$2 ]
then echo "\nTest passed"
else echo "\nTest failed"
fi
}

echo "### Create new books ###"
response=$(curl -i --data "title=new_book&author=pushkin&page_count=340&created=2016-12-31" -s http://localhost:8000/api/books/)
echo $response

test_status $response 201

# Get id of new book
id=$(echo $response | grep -Po '\"id\":\d+' | awk -F':' '{ print $2 }')

if [ -z $id ]
then id=0
fi

response=$(curl -i --data "title=second_book&author=fet&page_count=100&created=2016-12-31" -s http://localhost:8000/api/books/)
echo $response

test_status $response 201

# Get id of new second book
id2=$(echo $response | grep -Po '\"id\":\d+' | awk -F':' '{ print $2 }')

if [ -z $id2 ]
then id2=0
fi

echo "\n\n### Create book with title already exists (new_book) ###"
curl -i --data "title=new_book&author=tolstoy&page_count=340&created=2016-12-31" http://localhost:8000/api/books/

test_status $response 400

echo "\n\n### Create book without title (title is required) ###"
curl -i --data "title=&author=tolstoy&page_count=340&created=2016-12-31" http://localhost:8000/api/books/

test_status $response 400

echo "\n\n### Update title of the book with id=${id} to other title ###"
curl -i -X PUT --data "title=other_title" http://localhost:8000/api/books/${id}

test_status $response 200

echo "\n\n### Update title of the book with id=${id} to exist title (second_book)###"
curl -i -X PUT --data "title=second_book" http://localhost:8000/api/books/${id}

test_status $response 400

echo "\n\n### Update nonexistent book ###"
curl -i -X PUT --data "title=title" http://localhost:8000/api/books/6666

test_status $response 404

echo "\n\n### Delete book with id=${id} ###"
curl -i -X DELETE http://localhost:8000/api/books/${id}

test_status $response 204

echo "\n\n### Delete book with id=${id2} ###"
curl -i -X DELETE http://localhost:8000/api/books/${id2}

test_status $response 204

echo "\n\n### Delete nonexistent book ###"
curl -i -X DELETE http://localhost:8000/api/books/9999

test_status $response 404

echo "\n"