If pip is not installed:
$ python get-pip.py

If django not installed:
$ pip install Django==1.9.2

Installing rest_framework
$ pip install djangorestframework
$ pip install markdown
$ pip install django-filter

Clone repository:
$ git clone git@bitbucket.org:mandron/sstest.git


$ cd sstest

Launch django server:
$ ./manager.py runserver

Start tests:
$ sh test.sh

Through the browser
http://localhost:8000/api/books/