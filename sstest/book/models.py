from __future__ import unicode_literals

from django.db import models


class Book(models.Model):
    title = models.TextField(unique=True)
    author = models.TextField(blank=True)
    page_count = models.IntegerField(blank=True)
    created = models.DateField('date of publication', blank=True)