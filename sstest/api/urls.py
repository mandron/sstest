from django.conf.urls import url, include
from api import views

urlpatterns = [
    url(r'^books/$', views.books_list, name='books_get'),
    url(r'^books/(?P<id>[0-9]+)$', views.books_detail, name='books_detail'),
]